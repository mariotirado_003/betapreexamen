// Función para hacer la petición y mostrar todos los usuarios utilizando Axios
function obtenerUsuarios() {
    // URL de la API JSONPlaceholder para obtener usuarios
    const apiUrl = 'https://jsonplaceholder.typicode.com/users';

    // Realizar la petición utilizando Axios
    axios.get(apiUrl)
        .then(response => {
            // Limpiar la tabla de usuarios
            const tbodyUsuarios = document.getElementById('tbody-usuarios');
            tbodyUsuarios.innerHTML = '';

            // Mostrar todos los usuarios en la tabla
            response.data.forEach(usuario => {
                const row = tbodyUsuarios.insertRow();
                row.insertCell(0).textContent = usuario.id;
                row.insertCell(1).textContent = usuario.name;
                row.insertCell(2).textContent = usuario.username;
                row.insertCell(3).textContent = usuario.email;
                row.insertCell(4).textContent = usuario.address.street
                row.insertCell(5).textContent = usuario.address.suite;
                row.insertCell(6).textContent = usuario.address.city;
            });
        })
        .catch(error => {
            console.error('Error al obtener usuarios:', error);
        });
}

// Función para buscar un usuario por ID
function buscarUsuario() {
    const idUsuario = document.getElementById('idUsuario').value;
    const apiUrl = `https://jsonplaceholder.typicode.com/users/${idUsuario}`;

    // Realizar la petición utilizando Axios
    axios.get(apiUrl)
        .then(response => {
            // Mostrar el resultado de la búsqueda en los campos de texto
            const usuarioEncontrado = response.data;
            document.getElementById('txtNombre').value = usuarioEncontrado.name;
            document.getElementById('txtNombreUsuario').value = usuarioEncontrado.username;
            document.getElementById('txtEmail').value = usuarioEncontrado.email;
            document.getElementById('txtCalle').value = usuarioEncontrado.address.street;
            document.getElementById('txtNumero').value = usuarioEncontrado.address.suite;
            document.getElementById('txtCuidad').value = usuarioEncontrado.address.city;
        })
        .catch(error => {
            console.error('Error al buscar usuario por ID:', error);

            // Limpiar los campos de texto en caso de error
            document.getElementById('txtNombre').value = '';
            document.getElementById('txtNombreUsuario').value = '';
            document.getElementById('txtEmail').value = '';
            document.getElementById('txtCalle').value = '';
            document.getElementById('txtNumero').value = '';
            document.getElementById('txtCuidad').value = '';
        });
}

// Función para mostrar todos los usuarios
function mostrarTodos() {
    buscarUsuario();
}

// Llamar a la función cuando la página se carga
document.addEventListener('DOMContentLoaded', buscarUsuario);